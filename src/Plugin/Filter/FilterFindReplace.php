<?php

namespace Drupal\wysiwyg_find_replace_filter\Plugin\Filter;

use Drupal\Core\Form\FormInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\filter\FilterProcessResult;
use Drupal\filter\Plugin\FilterBase;

/**
 * @Filter(
 *   id = "filter_find_replace",
 *   title = @Translation("Filter Find and Replace from HTML"),
 *   type = Drupal\filter\Plugin\FilterInterface::TYPE_HTML_RESTRICTOR,
 *   settings = {},
 *   weight = -10
 * )
 */
class FilterFindReplace extends FilterBase {

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $form['find_text'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Find Text'),
      '#default_value' => $this->settings['find_text'],
      '#description' => $this->t('Find Text. Use (,) operator to find multiple texts.'),
    ];

    $form['replace_text'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Replace Text'),
      '#default_value' => $this->settings['replace_text'],
      '#description' => $this->t('Replacement Text. Use (,) operator to replace multiple texts.'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function setConfiguration(array $configuration) {
    if (isset($configuration['settings']['find_text'])) {
      $configuration['settings']['find_text'] = $configuration['settings']['find_text'];
    }
    if (isset($configuration['settings']['replace_text'])) {
      $configuration['settings']['replace_text'] = $configuration['settings']['replace_text'];
    }
    parent::setConfiguration($configuration);
  }

  /**
   * {@inheritdoc}
   */
  public function process($text, $langcode) {
    $str = explode(',', $this->settings['find_text']);
    $rplc = explode(',', $this->settings['replace_text']);
    $updatedHTML = str_replace($str, $rplc, $text);
    return new FilterProcessResult($updatedHTML);
  }


}
